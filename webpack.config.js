const path = require("path");
const webpack = require("webpack");

module.exports = [{
    entry: "./src/client.jsx",
    output: {
        filename: "client.min.js",
        path: path.join(__dirname, "/public/bundle"),
        publicPath: '/bundle/'
    },
		resolve: { // Resolves imports without suffixes like .js or .jsx
			extensions: [".js", ".jsx"]
		},
    module: {
      rules: [
        {
      		test: /\.jsx?$/,
      		exclude: /(node_modules|bower_components)/,
      		loader: "babel-loader",
    			options: {
    				presets: ["env", "react"],
    				plugins: [
    					'styled-components',
    					'transform-object-rest-spread'
    				]
    			},
        }
  		]
    }
  },{
		name: "server",
		target: "node",
		entry: "./src/server.jsx",
		output: {
			path: path.join(__dirname, 'static'),
			filename: 'server.js',
			libraryTarget: 'commonjs2',
			publicPath: '/static/',
		},
		resolve: {
			extensions: ['.js', '.jsx']
		},
		module: {
			rules: [
				{
					test: /\.jsx?$/,
					exclude: /(node_modules|bower_components)/,
					use: [
						{
							loader: "babel-loader",
							options: {
								presets: ["env", "react"],
								plugins: [
									'styled-components',
									'transform-object-rest-spread'
								]
							},
						}
					]
				},
			],
		},
	}
]