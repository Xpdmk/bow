# Sources
## Tutorials
#### Project setup

- React + ES6 + Webpack + Babel \
https://www.twilio.com/blog/2015/08/setting-up-react-for-es6-with-webpack-and-babel-2.html

- Minimal setup \
https://www.robinwieruch.de/minimal-react-webpack-babel-setup/



## CSS
#### Layout
- CSS Grid \
https://www.youtube.com/watch?v=7kVeCqQCxlk

## Javascript
#### Maintanance
- Javascript code guide \
https://github.com/airbnb/javascript

#### Sortable list/grid
- Sortable
https://github.com/RubaXa/Sortable

## React

#### Generate inline styling with Javascript
- styled-components \
https://www.youtube.com/watch?v=qu4U7lwZTRI
    - Tips and tricks
    https://github.com/styled-components/styled-components/blob/master/docs/tips-and-tricks.md

- Structuring \
https://m.alphasights.com/css-evolution-from-css-sass-bem-css-modules-to-styled-components-d4c1da3a659b

- ~~Radium\
https://www.youtube.com/watch?v=k3OF4A30jSQ (Features start at 20:30)~~

#### React testing
- Test utils:
https://facebook.github.io/react/docs/test-utils.html

- React documenting
gulp-react-docs

- React grid view
https://github.com/jrowny/react-absolute-grid



## HTML
### Drag and drop
- Drag and drop example: \
https://codepen.io/retrofuturistic/pen/tlbHE

- React Drag and drop: \
https://react-dnd.github.io/react-dnd/



## Webpack
#### Dev server
- Dev server setup: \
https://www.youtube.com/watch?v=soI7X-7OSb4