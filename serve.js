require("dotenv").config()
const express = require("express")
const app = express()
const serverRenderer = require("./static/server.js").default()

const port = process.env.PORT || 3000

// Serve /public for assets
app.use(express.static(__dirname + "/public"));

app.get("*", serverRenderer)

app.listen(port, () => console.log("Listening port " + port + "!"))