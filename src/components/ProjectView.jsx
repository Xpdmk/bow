import React from 'react';
import ReactDOM from 'react-dom';
import ProjectsContainer from './ProjectsContainer.jsx';

import projects from './../projects.json';

class ProjectView extends React.Component {
	render() {
		return (
			<div>
				<ProjectsContainer projects={projects} />
			</div>
		)
	}
}

ReactDOM.render(<ProjectView />, document.getElementById('app'));