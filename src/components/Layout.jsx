import React, {Component, Fragment} from "react"
import Navigation from "./../components/Navigation"

export default class Layout extends Component {
  render() {
    return (
      <Fragment>
        <Navigation theme={this.props.theme.navigation}/>
        {this.props.children}
      </Fragment>
    )
  }
}