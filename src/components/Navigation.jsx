import React, {Component} from "react"

const style = {
  height: "20px"
}

export default class Navigation extends Component {
  render() {
    const {theme} = this.props;
    return (
      <div style={{
        ...theme, 
        ...style
      }}></div>
    )
  }
}