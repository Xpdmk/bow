import React, {Compoent, Fragment} from 'react'
import { Link } from 'react-router'

export default class NotFoundPage extends React.Component {
  render() {
    return (
      <Fragment>
        <h1>404</h1>
        <h2>Page not found!</h2>
        <p>
          <Link to="/">Go back to the main page</Link>
        </p>
      </Fragment>)
  }
}