require("dotenv").config()
const express = require('express');
const path = require('path');
const app = express();
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackHotServerMiddleware = require('webpack-hot-server-middleware');
const config = require('./webpack.development.config.js');
const compiler = webpack(config);

const port = process.env.PORT_DEV || 9000

app.use(webpackDevMiddleware(compiler, {
	publicPath: "/public/",
}));
app.use(webpackHotMiddleware(compiler.compilers.find(compiler => compiler.name === 'client')));
app.use(webpackHotServerMiddleware(compiler));

app.listen(port, () => console.log("Listening port " + port + "! Building..."));