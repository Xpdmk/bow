const path = require("path");
const webpack = require("webpack");

module.exports = [
	{
		name: "client",
		target: "web",
		entry: "./src/client.jsx",
		output: {
			path: path.join(__dirname + "/public/bundle"),
			filename: "client.min.js",
			publicPath: "/bundle",
		},
		devtool: 'source-map',
		resolve: { // Resolves imports without suffixes like .js or .jsx
			extensions: [".js", ".jsx"]
		},
		module: {
			rules: [
				{
					test: /\.jsx?$/,
					exclude: /(node_modules|bower_components)/,
					use: [
						{
							loader: "babel-loader",
							options: {
								presets: ["react", "env"],
								plugins: [
									['styled-components', {
										"ssr": true,
									}],
									"transform-object-rest-spread"
								]
							},
						}
					]
				},
			]
		}
	},
	{
		name: "server",
		target: "node",
		entry: "./src/server.jsx",
		output: {
			path: path.join(__dirname, 'static'),
			filename: 'server.js',
			libraryTarget: 'commonjs2',
			publicPath: '/static/',
		},
		devtool: 'source-map',
		resolve: {
			extensions: ['.js', '.jsx']
		},
		module: {
			rules: [
				{
					test: /\.jsx?$/,
					exclude: /(node_modules|bower_components)/,
					use: [
						{
							loader: "babel-loader",
							options: {
								presets: ["env", "react"],
								plugins: [
									['styled-components', {
										"ssr": true,
									}],
									"transform-object-rest-spread"
								]
							},
						}
					]
				},
			],
		},
	}
];